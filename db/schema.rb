# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160507162601) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blacklists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "blacklist_user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "club_announcements", force: :cascade do |t|
    t.integer  "club_id"
    t.string   "title"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "club_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "club_id"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "club_comments", ["club_id"], name: "index_club_comments_on_club_id", using: :btree
  add_index "club_comments", ["user_id"], name: "index_club_comments_on_user_id", using: :btree

  create_table "club_extends", force: :cascade do |t|
    t.integer "club_id"
    t.string  "avatar"
    t.text    "description"
    t.integer "featured"
    t.string  "profile"
  end

  add_index "club_extends", ["club_id"], name: "index_club_extends_on_club_id", using: :btree

  create_table "club_submissions", force: :cascade do |t|
    t.integer  "club_id"
    t.integer  "submission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "club_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "club_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clubs", force: :cascade do |t|
    t.string   "name"
    t.integer  "owner_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  create_table "favourites_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "submission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "favourites_users", ["submission_id"], name: "index_favourites_users_on_submission_id", using: :btree
  add_index "favourites_users", ["user_id"], name: "index_favourites_users_on_user_id", using: :btree

  create_table "journal_comments", force: :cascade do |t|
    t.integer  "journal_id"
    t.integer  "user_id"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "journal_comments", ["journal_id"], name: "index_journal_comments_on_journal_id", using: :btree
  add_index "journal_comments", ["user_id"], name: "index_journal_comments_on_user_id", using: :btree

  create_table "journals", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "journals", ["user_id"], name: "index_journals_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "send_id"
    t.string   "title"
    t.text     "text"
    t.boolean  "deleted"
    t.boolean  "read"
    t.boolean  "send_deleted"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "news", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
  end

  add_index "news", ["user_id"], name: "index_news_on_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "tid"
    t.string   "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.string   "name"
    t.string   "readable_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "poll_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "poll_id"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "poll_comments", ["poll_id"], name: "index_poll_comments_on_poll_id", using: :btree
  add_index "poll_comments", ["user_id"], name: "index_poll_comments_on_user_id", using: :btree

  create_table "poll_options", force: :cascade do |t|
    t.integer  "poll_id"
    t.string   "options"
    t.string   "values"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "poll_options", ["poll_id"], name: "index_poll_options_on_poll_id", using: :btree

  create_table "poll_users", force: :cascade do |t|
    t.integer  "poll_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "poll_users", ["poll_id"], name: "index_poll_users_on_poll_id", using: :btree
  add_index "poll_users", ["user_id"], name: "index_poll_users_on_user_id", using: :btree

  create_table "polls", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "polls", ["user_id"], name: "index_polls_on_user_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "submission_id"
    t.text     "description"
    t.text     "reason"
    t.integer  "status"
    t.integer  "decision"
    t.integer  "decision_user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "uid"
  end

  add_index "reports", ["submission_id"], name: "index_reports_on_submission_id", using: :btree

  create_table "role_permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "role_users", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "readable_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "submission_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "submission_id"
    t.text     "comment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "submission_comments", ["submission_id"], name: "index_submission_comments_on_submission_id", using: :btree
  add_index "submission_comments", ["user_id"], name: "index_submission_comments_on_user_id", using: :btree

  create_table "submissions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "r18_rating"
    t.boolean  "general_rating"
    t.string   "tags"
    t.string   "filters"
    t.integer  "views"
    t.integer  "favourites"
    t.integer  "downloads"
    t.integer  "comments"
    t.string   "file_name"
    t.string   "file_name_thumbnail"
    t.string   "file_name_thumbboard"
    t.float    "size"
    t.string   "resolution"
    t.boolean  "reported"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "slug"
    t.boolean  "patreon"
  end

  add_index "submissions", ["user_id"], name: "index_submissions_on_user_id", using: :btree

  create_table "user_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "comment"
    t.integer  "commenter_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "user_comments", ["user_id"], name: "index_user_comments_on_user_id", using: :btree

  create_table "user_extends", force: :cascade do |t|
    t.integer "user_id"
    t.date    "dob"
    t.string  "fullname"
    t.string  "avatar"
    t.string  "id_image"
    t.string  "tzinfo"
    t.string  "language"
    t.string  "date_format"
    t.boolean "show_dob"
    t.boolean "show_name"
    t.boolean "show_email"
    t.string  "title"
    t.integer "featured"
    t.text    "profile_text"
    t.text    "signature"
    t.text    "patreon"
    t.text    "instagram"
    t.text    "gumroad"
    t.text    "da"
    t.text    "fa"
    t.text    "yg"
    t.date    "change_date"
    t.text    "change_history"
    t.integer "gender"
    t.boolean "deactivated"
  end

  add_index "user_extends", ["user_id"], name: "index_user_extends_on_user_id", using: :btree

  create_table "user_watches", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_watches", ["user_id"], name: "index_user_watches_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
