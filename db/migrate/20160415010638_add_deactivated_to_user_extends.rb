class AddDeactivatedToUserExtends < ActiveRecord::Migration[5.0]
  def change
    add_column :user_extends, :deactivated, :boolean
  end
end
