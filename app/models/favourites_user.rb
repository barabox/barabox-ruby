class FavouritesUser < ApplicationRecord
  belongs_to :user
  belongs_to :submission
end
