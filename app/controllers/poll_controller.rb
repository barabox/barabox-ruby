class PollController < ApplicationController

  before_action :require_login, except: ['polls']
  before_action :owner?, only: ['delete', 'edit']

  include PollHelper
  include NotificationHelper

  # Filters & Actions
  def owner?
    @poll = Poll.find_by_id(params[:id])
    if @current_user.id != @poll.user_id
      redirect_to root_url
    end
  end

  def polls
    @user = User.find_by_username(params[:user])
    @allpolls = @user.polls.all
  end

  def polls_create
    @options = params[:polls][:options].to_json
    @values = params[:polls][:values].to_json

    @poll = Poll.new
    @poll.user_id = @current_user.id
    @poll.title = params[:polls][:poll_title]
    @poll.text = params[:polls][:poll_text]
    @poll.save

    @po = PollOption.new
    @po.poll_id = @poll.id
    @po.options = @options
    @po.values = @values
    @po.save

    @w = UserWatch.where('uid = ?', @current_user.id)
    @w.each do |watch|
      createNotification(6, {type: 'journal', id: @po.id, from: @current_user.id}, watch.user.id)
    end

    redirect_to view_polls_url @current_user.username
  end

  def delete
    @poll = Poll.find_by_id(params[:id])
    @poll.destroy
    redirect_to view_polls_url(@current_user.username)
  end

  def edit
    @user = User.find_by_username(params[:user])
    @poll = Poll.find_by_id(params[:id])
  end

  def update
    id = params[:polls][:poll_id]
    @poll = Poll.find_by_id(id)
    @poll.update_attribute('title', params[:polls][:poll_title])
    @poll.update_attribute('text', params[:polls][:poll_text])
    @poll.poll_option.update_attribute('options', params[:polls][:options].to_json)
    redirect_to view_polls_path(@current_user.username)
  end
end
