class UserSettingsController < ApplicationController
  before_action :require_login

  def index
  end

  def personal
  end

  def integration

  end

  def integration_patch
    @u = User.find_by_id(@current_user.id)
    @u.user_extend.update(integration_params)
    redirect_to user_settings_integration_path
  end

  def integration_params
    params.require(:user_extend).permit(:patreon)
  end

  def personalinfoupdate
    user = UserExtend.find_by(user_id: @current_user.id)
    user.update_attribute(:fullname, params[:user_extend][:fullname])
    user.update_attribute(:show_dob, params[:user_extend][:show_dob])
    user.update_attribute(:gender, params[:user_extend][:gender]);
    user.update_attribute(:show_name, params[:user_extend][:show_name])
    user.update_attribute(:show_email, false)
    redirect_to user_settings_personal_url
  end

  def avatarupdate
    user = UserExtend.find_by(user_id: @current_user.id)
    user.update_attribute(:avatar, params[:avatar_file])
    redirect_to user_settings_url
  end

  def signatureupdate
    signature = params[:user_extend][:signature]
    user = UserExtend.find_by(user_id: @current_user.id)
    user.update_attribute(:signature, signature)
    redirect_to user_settings_url
  end

end
