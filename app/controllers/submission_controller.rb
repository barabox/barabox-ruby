class SubmissionController < ApplicationController

  include DateAndTime
  include PaginationHelper
  include NotificationHelper
  include FavouritesHelper
  include SubmissionHelper
  include IntegrationHelper

  before_action :require_login, except: ['view']

  def search

  end

  def new
  end

  def upload
    submission = Submission.new
    submission.user_id = @current_user.id
    submission.title = params[:submission][:title]
    submission.description = params[:submission][:description]
    submission.r18_rating = (params[:submission][:rating] == 'r18') ? 1 : 0
    submission.general_rating = 1
    submission.tags = params[:submission][:tags]
    submission.filters = nil
    submission.file_name = params[:submission][:submission_image]
    submission.patreon = params[:submission][:patreon]

    # Default to 0
    submission.views = 0
    submission.favourites = 0
    submission.downloads = 0
    submission.comments = 0
    submission.reported = false

    filter_array = []
    if !params[:filter][:violence].nil?
      filter_array.push 'violence'
    end

    if !params[:filter][:drugs].nil?
      filter_array.push 'drugs'
    end

    if !params[:filter][:language].nil?
      filter_array.push 'language'
    end

    if !params[:filter][:crime].nil?
      filter_array.push 'crime'
    end

    if !params[:filter][:furry].nil?
      filter_array.push 'furry'
    end

    submission.filters = filter_array.to_json

    if submission.save
      slug = params[:submission][:title].parameterize
      submission.update_attribute('slug', slug + '-' + submission.id.to_s)

      clubs = params[:submission][:club]
      if !clubs.nil?
        clubs.each do |k, v|
          @club = Club.find_by_id(v)
          @club.club_submissions.create(submission_id: submission.id)
        end
      end

      # Send out Notifications based on Users Watch list....
      @w = UserWatch.where('uid = ?', @current_user.id)
      @w.each do |watch|
        createNotification(5, {type: 'art', id: submission.id, slug: slug}, watch.user_id)
      end

      redirect_to submission_view_path(submission.slug)
    else
      submission.errors.full_messages.each do |message|
        flash[:danger] = message
      end
      redirect_to submission_create_url
    end
  end

  def view
    @submission = Submission.find_by_slug(params[:slug])
    @tags = @submission.tags.split(',')

    paginationCreate(25, SubmissionComment, 'submission_id', @submission.id)

    @resolution = @submission.resolution.split(';')

    @download = @submission.file_name.url

    @submission.update_attribute('views', (@submission.views + 1))

    # If not Exist... go to homepage
    if @submission.nil?
      redirect_to root_url
    end

  end

  def newcomment

    @s = Submission.find_by_id(params[:submission_comments][:submission_id])

    @sc = SubmissionComment.new
    @sc.user_id = @current_user.id
    @sc.submission_id = params[:submission_comments][:submission_id]
    @sc.comment = params[:submission_comments][:comment]
    if @sc.save
      modCommentCount(@s, 1)
      @sxc = SubmissionComment.where('submission_id = ?', @s.id)
      pagec = @sxc.count / 25;
      createNotification(2, {type: 'art', id: @s.id, from: @current_user.id, cid: @sc.id, page: pagec}, @s.user.id)
      redirect_to submission_view_url(@s.slug)
    else

    end
  end

  def favourite
    id = Submission.find_by_slug(params[:slug]).id
    @s = Submission.find_by_slug(params[:slug])
    uid = @current_user.id

    # Check if Already Exists...
    @fc = FavouritesUser.where(:user_id => uid, :submission_id => id)
    if @fc.empty?
      @f = FavouritesUser.new
      @f.user_id = uid
      @f.submission_id = id
      @f.save
      createNotification(8, {type: 'art', id: id, from: uid}, @s.user.id)
      modSubmissionFavourites(@s, 1)
    else
      FavouritesUser.where(:user_id => uid, :submission_id => id).destroy_all
      deleteNotification(8, {type: 'art', id: id, from: uid})
      modSubmissionFavourites(@s, -1)
    end

    redirect_to submission_view_path(params[:slug])
  end

  def report
    @s = Submission.find_by_slug(params[:slug])

    # Do not allow multiple reports
    if !@s.report.blank?
      redirect_to submission_view_path(@s.slug)
    end

  end

  def submitreport
    @s = Submission.find_by_slug(params[:slug])
    @r = Report.new
    @r.submission_id = @s.id
    @r.description = params[:report_submission][:report_desc]
    @r.reason = params[:report_submission][:reason].to_json
    @r.uid = @current_user.id
    if @r.save
      createNotification(0, {type: 'art', id: @s.id}, @s.user.id)
      @s.update_attribute('reported', true)
    end
  end

end
