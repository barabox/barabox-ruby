module SessionHelper
  def login_user(user)
    session[:user_id] = user.id
  end

  def current_user
    @current_user = User.find_by(id: session[:user_id])
  end

  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  def pluralize?(word, cnt)
    if cnt == 1
      return "#{cnt} " + word.singularize
    elsif cnt == 0
      return "No " + word.pluralize
    else
      return "#{cnt} " + word.pluralize
    end
  end

  def avatar
    if logged_in?
      @avatar = User.find_by_id(@current_user.id).user_extend.avatar
    end
  end
end
