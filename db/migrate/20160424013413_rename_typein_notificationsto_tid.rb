class RenameTypeinNotificationstoTid < ActiveRecord::Migration[5.0]
  def change
    rename_column :notifications, :type, :tid
  end
end
