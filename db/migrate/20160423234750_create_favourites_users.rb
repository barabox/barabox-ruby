class CreateFavouritesUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :favourites_users do |t|
      t.belongs_to :user
      t.belongs_to :submission
      t.timestamps
    end
  end
end
