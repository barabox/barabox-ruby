class ApiController < ApplicationController
  def submission_download
    id = params[:id]
    @s = Submission.find_by_id(id)
    @s.update_attribute('downloads', @s.downloads + 1)
  end
end
