module UserSettingsHelper
  def isActive(cntl,act)
    if current_page?(:controller => cntl, :action => act)
      return "class = active"
    end
  end

  private
  def showBirthday?
    if @current_user.user_extend.show_dob
      return 1
    else
      return 0
    end
  end

  def showName?
    if @current_user.user_extend.show_name
      return 1
    else
      return 0
    end
  end
end
