class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.belongs_to :user
      t.integer :send_id
      t.string  :title
      t.text  :text
      t.boolean :deleted
      t.boolean :read
      t.boolean :send_deleted
      t.timestamps
    end
  end
end
