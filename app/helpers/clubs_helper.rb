module ClubsHelper

  def isMember?
    @club = Club.find_by_id(params[:id])
    @user = @club.club_users.find_by(user_id: @current_user.id)
    return @user.nil?
  end

  def isWatching?

  end

  def isOwner?
    id = @current_user.id
    return (Club.find_by(id: params[:id]).owner_id === id)
  end
end
