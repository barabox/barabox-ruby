class RemoveValuesFromPolls < ActiveRecord::Migration[5.0]
  def change
    remove_column :polls, :values, :string
  end
end
