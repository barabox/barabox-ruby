class JournalController < ApplicationController

  before_action :require_login, :except => [:index, :view]
  before_action :owner?, :only => [:edit, :update, :delete]

  include SessionHelper
  include JournalHelper
  include PaginationHelper
  include ApplicationHelper
  include NotificationHelper

  def owner?
    id = params[:id]
    @j = Journal.find_by_id(id)
    if @j.user_id != @current_user.id
      return false
    end
    return true
  end

  def index
    @user = User.find_by_username(params[:user])
    max = 5
    @journals = @user.journals.limit(max).order(id: :desc)
    genPaginationCreate(max, Journal)
  end

  def view
    @user = User.find_by_username(params[:user])
    @journal = Journal.find_by_id(params[:id])
    if @journal.nil?
      redirect_to view_journals_path(@user.username)
    end

    paginationCreate(20, JournalComment, 'journal_id', @journal.id)
  end

  def edit
    @user = User.find_by_username(params[:user])
    @journal = Journal.find_by_id(params[:id])
  end

  def update
    @user = User.find_by_username(params[:user])
    @journal = Journal.find_by_id(params[:id])

    @journal.update_attribute('title', params[:journal][:title])
    @journal.update_attribute('text', params[:journal][:text])

    redirect_to edit_journal_path(@user.username, @journal.id)
  end

  def new
    @j = Journal.new
    @j.user_id = @current_user.id
    @j.title = params[:journal][:title]
    @j.text = params[:journal][:text]
    if @j.save
      @w = UserWatch.where('uid = ?', @current_user.id)
      @w.each do |watch|
        createNotification(4, {type: 'journal', id: @j.id, from: @current_user.id}, watch.user.id)
      end
      redirect_to view_journals_path(params[:user])
    else
      redirect_to view_journals_path(params[:user])
      @j.errors.full_messages.each do |message|
        flash[:danger] = message
      end
    end
  end

  def delete
    @j = Journal.find_by_id(params[:id])
    @j.destroy
    redirect_to view_journals_path(params[:user])
  end

  def comment
    @jc = JournalComment.new
    @jc.journal_id = params[:id]
    @jc.user_id = @current_user.id
    @jc.text = params[:journal_comment][:comment]
    @jc.save
    redirect_to view_journal_path(params[:journal_comment][:username], params[:id])
  end
end
