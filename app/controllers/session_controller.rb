class SessionController < ApplicationController
  def new
  end

  def create
    if (!params.has_key?(:session))
      redirect_to root_url
    else
      user = User.where('LOWER(username) = ?', params[:session][:username].downcase).first
      if user && user.authenticate(params[:session][:password_digest])
        login_user user
        redirect_to root_url
      else
        flash.now[:danger] = "Invalid username / password combination!"
        redirect_to root_url
      end

    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
