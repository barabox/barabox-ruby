module SubmissionHelper
  def modCommentCount(model, mod)
    model.update_attribute('comments', model.comments + mod)
  end
end
