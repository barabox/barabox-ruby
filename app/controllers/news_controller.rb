class NewsController < ApplicationController

  before_action :require_login, only: [:new, :create, :edit, :update, :destroy]
  before_action :checkAdmin?, except: [:index, :show]

  def index
    @articles = News.all.limit(10).order(id: :desc)
  end

  def new
    @article = News.new
  end

  def create
    @article = News.new(news_params)
    @article.slug = @article.title.parameterize
    if @article.save
      redirect_to news_view_path(@article.slug)
    end
  end

  def show
    @article = News.find_by_slug(params[:slug])
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def news_params
    params.require(:news).permit(:title, :text)
  end
end
