# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  rImage = null

  $('#avatar-open').click (e) ->
    e.preventDefault()
    $('#avatar-upload').trigger 'click'
    return

  $('.announcement-display').each ->
    if $('.text').height() >= 350
      $('.gradient-hide').removeClass 'hide'
    return

  $('#avatar-upload').change ->
    image = new Image
    canvas = $('#club-avatar')
    if this.files && this.files[0]
      reader = new FileReader

      reader.onload = (e) ->
        image.src = e.target.result
        canvas.removeClass('hide')
        canvas.css('background-image', 'url('+image.src+')')
        return
      reader.readAsDataURL(this.files[0])
    return
  return
