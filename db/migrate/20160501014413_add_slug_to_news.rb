class AddSlugToNews < ActiveRecord::Migration[5.0]
  def change
    add_column :news, :slug, :string
  end
end
