require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get notifications_show_url
    assert_response :success
  end

  test "should get watch" do
    get notifications_watch_url
    assert_response :success
  end

  test "should get art" do
    get notifications_art_url
    assert_response :success
  end

  test "should get journals" do
    get notifications_journals_url
    assert_response :success
  end

  test "should get polls" do
    get notifications_polls_url
    assert_response :success
  end

  test "should get comments" do
    get notifications_comments_url
    assert_response :success
  end

  test "should get reports" do
    get notifications_reports_url
    assert_response :success
  end

end
