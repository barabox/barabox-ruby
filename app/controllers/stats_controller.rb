class StatsController < ApplicationController
  def index
    @user = User.find_by_username(params[:user])

    @commentGraph = Submission.order(comments: :desc).limit(5)
  end
end
