class CreatePollOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :poll_options do |t|
      t.belongs_to :poll
      t.string :options
      t.string :values
      t.timestamps
    end
  end
end
