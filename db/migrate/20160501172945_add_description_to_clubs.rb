class AddDescriptionToClubs < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :description, :text
  end
end
