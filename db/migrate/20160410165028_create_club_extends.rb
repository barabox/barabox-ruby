class CreateClubExtends < ActiveRecord::Migration[5.0]
  def change
    create_table :club_extends do |t|
      t.belongs_to  :club

      t.string  :avatar
      t.text  :description
      t.integer :featured
    end
  end
end
