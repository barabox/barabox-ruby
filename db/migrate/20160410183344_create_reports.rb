class CreateReports < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.belongs_to  :submission
      t.text  :description
      t.text  :reason
      t.integer :status
      t.integer :decision
      t.integer :decision_user_id
      t.timestamps
    end
  end
end
