class AddGenderToUserExtends < ActiveRecord::Migration[5.0]
  def change
    add_column :user_extends, :gender, :integer
  end
end
