class AddUidToReports < ActiveRecord::Migration[5.0]
  def change
    add_column :reports, :uid, :integer
  end
end
