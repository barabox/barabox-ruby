class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string  :name
      t.string  :readable_name
      t.timestamps
    end

    Role.create(:name => 'admin', :readable_name => 'Administrator')
    Role.create(:name => 'supermod', :readable_name => 'Super Moderator')
    Role.create(:name => 'mod', :readable_name => 'Moderator')
    Role.create(:name => 'helpdesk', :readable_name => 'Help Desk')
    Role.create(:name => 'user', :readable_name => 'User')
  end
end
