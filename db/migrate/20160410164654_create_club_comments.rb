class CreateClubComments < ActiveRecord::Migration[5.0]
  def change
    create_table :club_comments do |t|
      t.belongs_to  :user
      t.belongs_to  :club
      t.text  :comment
      t.timestamps
    end
  end
end
