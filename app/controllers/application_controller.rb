class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Helpers
  include SessionHelper
  include GravatarHelper
  include PermissionHelper
  include UserSettingsHelper
  include MailboxHelper

  def require_login
    if logged_in?

    else
      redirect_to root_url
    end
  end

  def checkAdmin?
    if !logged_in?
      return false
    end
    if isAdmin?
      return true
    end
    redirect_to root_url
  end

  def activePage?(controller, action)
    if (controller == params[:controller] && action == params[:action])
      return "class = active"
    end
    return false
  end

  def dateFormat(datetime, format = nil)
    if format.nil?
      return datetime.strftime("%m/%d/%Y")
    else
      return datetime.strftime(format)
    end
  end


  helper_method :activePage?
  helper_method :checkAdmin?
  helper_method :dateFormat

end
