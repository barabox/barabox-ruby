class IndexController < ApplicationController

  include GravatarHelper

  def index
    @user = User.new
    @s = Submission.limit(20)

    @top16 = @s.order(id: :desc)
    @popular = @s.order(views: :desc).limit(8)
    @journals = Journal.limit(9).order(id: :desc)
    @users = User.limit(12).order(id: :desc)
    @popularUsers = UserWatch.select('count(user_id) as guid, user_id').group('user_id').order('guid DESC').limit(6)
  end

  def aaa

  end

end
