class Club < ApplicationRecord
  has_many :club_users
  has_many :users, :through => :club_users
  has_many :club_comments
  has_many :club_submissions
  has_many :club_announcements
  has_one :club_extend
end
