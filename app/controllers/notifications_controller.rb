class NotificationsController < ApplicationController

  before_action :require_login

  include PaginationHelper

  def show
    max = 15
    @offset = @offset = (params[:offset].nil?) ? 0 : params[:offset]
    @messages = @current_user.messages.limit(max).offset(@offset).order(id: :desc)
    genPaginationCreate(max, @current_user.messages)
  end

  def watch
    @reports = @current_user.notifications.where('tid = ?', 1)
  end

  def art
    @reports = @current_user.notifications.where('tid = ?', 5)
  end

  def journals
    @reports = @current_user.notifications.where('tid = ?', 4)
  end

  def polls
    @reports = @current_user.notifications.where('tid = ?', 6)
  end

  def comments
    @reports = @current_user.notifications.where('tid = ?', 2)
  end

  def reports
    @reports = @current_user.notifications.where('tid = ?', 0)
  end

  def read
    max = 15
    @offset = @offset = (params[:offset].nil?) ? 0 : params[:offset]
    @messages = @current_user.messages.limit(max).offset(@offset).order(id: :desc)
    genPaginationCreate(max, @current_user.messages)
    @message = Message.find_by_id(params[:id])
    @sender = User.find_by_id(@message.send_id)
  end

  def favs
    @reports = @current_user.notifications.where('tid = ?', 8)
  end

  def pmdelete
    messages = params[:message]
    messages.each do |k,v|
      @m = Message.where('id = ?', v)
      @m.destroy_all
    end
    redirect_to notifications_show_path
  end

  def delete
    id = params[:id]
    @r = Notification.find_by_id(id)

    if @current_user.id === @r.user_id
      @r.delete
    end

    redirect_to :back

  end
end
