class ClubSubmission < ApplicationRecord
  belongs_to :submission
  belongs_to :club
end
