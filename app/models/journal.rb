class Journal < ApplicationRecord
  belongs_to :user
  has_many :journal_comments, dependent: :destroy

  validates :title, :allow_nil => false, :length => {minimum: 1, maximum: 32}
  validates :text, :allow_nil => false, :length => {minimum: 1}
end
