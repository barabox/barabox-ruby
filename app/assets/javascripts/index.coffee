$(document).on 'turbolinks:load', ->
  $('#user-login').click (e) ->
    e.preventDefault()
    $('#form-login').submit()
    return
  $('#user-signup').click (e) ->
    e.preventDefault()
    $('#form-register').submit()
    return
  $('textarea').summernote()
  return