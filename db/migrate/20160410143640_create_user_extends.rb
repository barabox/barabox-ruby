class CreateUserExtends < ActiveRecord::Migration[5.0]
  def change
    create_table :user_extends do |t|

      t.belongs_to  :user

      t.date  :dob
      t.string  :fullname
      t.string :avatar
      t.string  :id_image

      t.string  :tzinfo
      t.string :language
      t.string :date_format

      t.boolean :show_dob
      t.boolean :show_name
      t.boolean :show_email

      t.string  :title
      t.integer :featured

      t.text  :profile_text
      t.text  :signature

      # Social Media Links
      t.text  :patreon
      t.text  :instagram
      t.text  :gumroad
      t.text  :da
      t.text  :fa
      t.text  :yg

      # Change Username
      t.date  :change_date
      t.text  :change_history
    end
  end
end
