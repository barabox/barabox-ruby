# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  options = 0
  max = 10

  $("#add-poll-option").click (e) ->
    e.preventDefault()
    if options < max
      $("#option-container").append(addOption())
      options++
    return

  addOption = ->
    html = '<div class="form-group">'
    html += '<label>Option ' + (options + 1 ) + '</label>'
    html += '<input type="hidden" name="polls[values][value_'+options+'" value="0">'
    html += '<input class="form-control" type="text" name="polls[options][option_'+options+']" id="polls_options_'+options+'">'
    html += '</div>'
    return html
    return
