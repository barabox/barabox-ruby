class CreateJournalComments < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_comments do |t|
      t.belongs_to  :journal
      t.belongs_to  :user
      t.text  :text
      t.timestamps
    end
  end
end
