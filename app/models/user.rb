class User < ApplicationRecord
  has_secure_password
  has_many :club_users
  has_many :user_comments
  has_many :clubs, :through => :club_users
  has_many :journals
  has_many :polls
  has_many :roles, :through => :role_users
  has_many :role_users
  has_many :blacklists
  has_many :favourites_users
  has_many :notifications
  has_many :user_watches
  has_many :messages
  has_many :submissions
  has_many :newses
  has_one :user_extend

  # Validation
  validates :username, presence: true, length: {minimum: 6, maximum: 12}, allow_nil: false, uniqueness: true
  validates :password, presence: true, confirmation: true, allow_nil: false, length: {minimum: 6, maximum: 16}
  validates :email, presence: true, uniqueness: true
end
