module GravatarHelper
  require 'digest/md5'

  def grabGravatar(email)
    # Grab Avatar or Gravatar
    user = User.find_by(email: email)
    if user.user_extend.avatar.blank?
      email = email.downcase
      hash = Digest::MD5.hexdigest(email)
      image_src = "http://www.gravatar.com/avatar/#{hash}"
      return image_src
    else
      return root_url + "#{user.user_extend.avatar}"
    end
  end
end
