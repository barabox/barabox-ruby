class CreateClubAnnouncements < ActiveRecord::Migration[5.0]
  def change
    create_table :club_announcements do |t|
      t.integer :club_id
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
