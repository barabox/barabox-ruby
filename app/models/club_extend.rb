class ClubExtend < ApplicationRecord
  belongs_to :club
  mount_uploader :avatar, ClubavatarUploader
end
