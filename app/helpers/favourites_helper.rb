module FavouritesHelper

  def modSubmissionFavourites(model,mod)
    model.update_attribute('favourites', model.favourites + mod)
  end

end
