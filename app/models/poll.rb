class Poll < ApplicationRecord
  belongs_to :user
  has_many :poll_users, dependent: :destroy
  has_many :poll_comments, dependent: :destroy
  has_one :poll_option, dependent: :destroy
end
