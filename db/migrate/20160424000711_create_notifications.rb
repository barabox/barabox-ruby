class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :user
      t.integer :type # 0 = Report, 1 = Watch, 2 = Comment, 3 = Note, 4 = Journal, 5 = Art, 6 = Poll, 7 = News, 8 = Favourite
      t.string :data
      t.timestamps
    end
  end
end
