class CreatePollComments < ActiveRecord::Migration[5.0]
  def change
    create_table :poll_comments do |t|
      t.belongs_to  :user
      t.belongs_to  :poll
      t.text  :text
      t.timestamps
    end
  end
end
