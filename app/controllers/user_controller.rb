class UserController < ApplicationController

  before_action :require_login, :only => [:watch, :blacklist]

  include PaginationHelper
  include SessionHelper
  include NotificationHelper

  def create
    # Check if 18!
    birthday = params[:birthday]
    d = Date.new(birthday['(1i)'].to_i, birthday['(2i)'].to_i, birthday['(3i)'].to_i)
    c = Date.today
    if (c.year - d.year < 18)
      flash[:danger] = 'You must be 18 or older to join this site'
    end

    # Check if Accepted TOS
    accept_tos = params[:accept_tos]
    if accept_tos.nil?
      flash[:danger] = 'You must accept our TOS'
    end

    @user = User.new(user_params)
    if @user.save
      @user.role_users.create(user_id: @user.id, role_id: 5)
      @user.create_user_extend(user_id: @user.id, dob: d)
      @userid = login_user @user
    else
      @user.errors.full_messages.each do |message|
        flash[:danger] = message
      end
    end

    redirect_to root_url

  end

  def view
    @user = User.find_by_username(params[:user])
    @last5 = Submission.where('user_id' => @user.id).order(id: :desc).limit(5)
    @journal = @user.journals.order(id: :desc).limit(1)[0]

    paginationCreate(10, UserComment, 'user_id', @user.id)
  end

  def gallery
    @user = User.find_by_username(params[:user])
    @gallery = Submission.where('user_id = ?', @user.id).order(created_at: :desc)
    @count = @gallery.count
  end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end

  def watch
    if params[:user] === @current_user.username
      redirect_to view_user_path(params[:user])
    end

    # Check if Exists...
    @u = User.find_by_username(params[:user])
    @w = UserWatch.where('user_id = ? AND uid = ?', @current_user.id, @u.id)
    if @w.blank? || @w.empty?
      model = UserWatch.new
      model.user_id = @current_user.id
      model.uid = @u.id
      if model.save
        createNotification(1, {type: 'watch', user: @u.username, from: @current_user.id}, @u.id)
      end
    else
      if @w.delete_all
        deleteNotification(1, {type: 'watch', user: @u.username, from: @current_user.id})
      end
    end

    redirect_to view_user_path(@u.username)

  end

  def blacklist
    if params[:user] === @current_user.username
      redirect_to view_user_path(params[:user])
    end

    # Check if Exists...
    @u = User.find_by_username(params[:user])
    @b = Blacklist.where('user_id = ? AND blacklist_user_id = ?', @current_user.id, @u.id)
    if @b.blank? || @b.empty?
      model = Blacklist.new
      model.user_id = @current_user.id
      model.blacklist_user_id = @u.id
      model.save
    else
      @b.delete_all

    end
    redirect_to view_user_path(@u.username)
  end

  def sendmessage
    @u = User.find_by_username(params[:message][:recipient])
    uid = params[:message][:uid]

    @m = Message.new
    @m.user_id = @u.id
    @m.send_id = uid
    @m.title = params[:message][:title]
    @m.text = params[:message][:text]
    @m.deleted = false
    @m.read = false
    @m.send_deleted = false
    if @m.save
      redirect_to view_user_path(@u.username)
    end
  end

  def favourites
    @user = User.find_by_username(params[:user])
    @favourites = FavouritesUser.where('user_id = ?', @user.id)
  end
end
