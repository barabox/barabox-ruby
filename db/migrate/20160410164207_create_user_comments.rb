class CreateUserComments < ActiveRecord::Migration[5.0]
  def change
    create_table :user_comments do |t|
      t.belongs_to  :user
      t.text  :comment
      t.integer :commenter_id
      t.timestamps
    end
  end
end
