module FakerHelper
  def commentfaker
    max = 100
    max.times do
      record = ClubComment.new
      record.user_id = 1
      record.club_id = 2
      record.comment = Faker::Lorem.sentence
      record.save
    end
  end

  def usercommentfaker
    max = 100
    max.times do
      record = UserComment.new
      record.user_id = 1
      record.commenter_id
      record.comment = Faker::Lorem.sentence
      record.save
    end
  end

  def pollsfaker

    options = {
        'option_0':'0',
        'option_1':'1',
        'option_2':'2',
        'option_3':'3',
        'option_4':'4',
        'option_5':'5',
        'option_6':'6',
        'option_7':'7',
        'option_8':'8',
        'option_9':'9',
    }
    values = {
        'value_0':0,
        'value_1':1,
        'value_2':2,
        'value_3':3,
        'value_4':4,
        'value_5':5,
        'value_6':6,
        'value_7':7,
        'value_8':8,
        'value_9':9,

    }

    max = 5
    max.times do
      record = Poll.new
      record.user_id = 1
      record.title = Faker::Lorem.word
      record.text = Faker::Lorem.paragraph
      record.save
      recordpo = PollOption.new
      recordpo.poll_id = record.id
      recordpo.options = options.to_json
      recordpo.values = values.to_json
      recordpo.save
    end
  end

  def addToPolls
    record = PollOption.find_by_id(16)
    values = JSON.parse record.values
    (0..9).each do |i|
      values["value_"+i.to_s] = 5
    end
    record.update_attribute('values', values.to_json)
  end

  def fakerJournal
    max = 100
    max.times do
      record = Journal.new
      record.user_id = 1
      record.title = Faker::Lorem.word
      record.text = Faker::Lorem.sentence
      record.save
    end
  end

  def fakerMessages
    max = 100
    max.times do
      record = Message.new
      record.user_id = 1
      record.send_id = 1
      record.title = Faker::Lorem.word
      record.text = Faker::Lorem.paragraph
      record.deleted = false
      record.read = false
      record.send_deleted = false
      record.save
    end
  end
end
