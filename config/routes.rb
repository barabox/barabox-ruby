Rails.application.routes.draw do
  get 'clubs/index'

  get 'clubs/show'

  get 'clubs/edit'

  get 'clubs/create'

  get 'clubs/new'

  get 'clubs/delete'

  get 'clubs/gallery'

  get 'clubs/users'

  get 'news/index'

  get 'news/new'

  get 'news/create'

  get 'news/show'

  get 'news/edit'

  get 'news/update'

  get 'news/destroy'

  get 'notifications/reports'

  get 'notifications/show'

  get 'notifications/notices'

  get 'notifications/watch'

  get 'notifications/art'

  get 'notifications/journals'

  get 'notifications/polls'

  get 'notifications/comments'

  get 'admin/index'

  get 'stats/index'

  get 'journal/index'

  get 'journal/view'

  get 'journal/edit'

  get 'journal/update'

  get 'journal/new'

  get 'journal/delete'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
  root 'index#index'

  post 'search', to: 'submission#search', as: 'submission-submit'
  post 'login' => 'session#create'
  get 'login' => 'session#create'
  post 'register' => 'user#create'
  delete 'logout' => 'session#destroy'

  get '/user/settings' => 'user_settings#index'
  get '/user/settings/personal' => 'user_settings#personal'
  get '/user/settings/integration' => 'user_settings#integration'
  get '/user/settings/deactivate' => 'user_settings#deactivate'
  post '/user/settings/avatar-update' => 'user_settings#avatarupdate'
  post '/user/settings/signature-update' => 'user_settings#signatureupdate'
  post '/user/settings/personalinfo-update' => 'user_settings#personalinfoupdate'
  get '/user/:user', to: 'user#view', as: 'user_view'
  patch '/user/settings/integration' => 'user_settings#integration_patch'

  # Submission
  get '/submission', to: 'submission#new', as: 'submission_create'
  get '/art/:slug', to: 'submission#view', as: 'submission_view'
  post '/submission', to: 'submission#upload', as: 'submission_upload'
  post '/submission/add-comment', to: 'submission#newcomment', as: 'submission_addcomment'
  patch '/art/:slug/favourite', to: 'submission#favourite', as: 'submission_favourite'
  get '/art/:slug/report', to: 'submission#report', as: 'submission_report'
  post '/art/:slug/report', to: 'submission#submitreport', as: 'submission_submitreport'

  # Submission API
  get '/api/submission/downloads/:id', to: 'api#submission_download', as: 'api_submission_download'

  # Tag Search
  get '/tag/:tag', to: 'search#tag', as: 'search_tag'

  # User
  get '/user/:user', to: 'user#view', as: 'view_user'
  get '/user/:user/gallery', to: 'user#gallery', as: 'view_gallery'

  get '/user/:user/favourites', to: 'user#favourites', as: 'view_favourites'

  get '/user/:user/watch', to: 'user#watch', as: 'watch_user'
  get '/user/:user/blacklist', to: 'user#blacklist', as: 'blacklist_user'

  get '/user/:user/polls', to: 'poll#polls', as: 'view_polls'
  post '/user/:user/polls', to: 'poll#polls_create', as: 'post_polls'
  post '/user/:user/poll/:id', to: 'poll#vote', as: 'vote_polls'
  delete '/user/:user/poll/:id', to: 'poll#delete', as: 'delete_polls'
  get 'user/:user/poll/edit/:id', to: 'poll#edit', as: 'edit_polls'
  patch 'user/:user/poll/edit/:id', to: 'poll#update', as: 'update_polls'

  get '/user/:user/journal', to: 'journal#index', as: 'view_journals'
  get '/user/:user/journal/:id', to: 'journal#view', as: 'view_journal'
  get '/user/:user/journal/edit/:id', to: 'journal#edit', as: 'edit_journal'
  patch '/user/:user/journal/edit/:id', to: 'journal#update', as: 'update_journal'
  post '/user/:user/journal/create', to: 'journal#new', as: 'create_journal'
  delete '/user/:user/journal/:id', to: 'journal#delete', as: 'delete_journal'
  post '/journal/comment/:id', to: 'journal#comment', as: 'comment_journal'

  get '/user/:user/stats', to: 'stats#index', as: 'view_stats'
  post '/send/message', to: 'user#sendmessage', as: 'send_message'

  # Admin
  get '/admin', to: 'admin#index', as: 'index_admin'

  # Notifications
  get '/notifications', to: 'notifications#show'
  get '/notifications/read/:id', to: 'notifications#read', as: 'read_pm'
  post '/notifications/pm/delete', to: 'notifications#pmdelete', as: 'delete_pm'
  post '/notifications/delete/:id', to: 'notifications#delete', as: 'delete_notification'
  get '/notifications/favs', to: 'notifications#favs', as: 'notification_favs'

  # News
  post '/news/create', to: 'news#create'
  get '/news/show/:slug', to: 'news#show', as: 'news_view'

  # Clubs
  get '/clubs', to: 'clubs#index', as: 'club_index'
  get '/club/:id', to: 'clubs#show', as: 'club_show'
  get '/clubs/new', to: 'clubs#new', as: 'club_new'
  post '/clubs/create', to: 'clubs#create', as: 'club_create'
  get '/club/:id/gallery', to: 'clubs#gallery', as: 'club_gallery'
  get '/club/:id/members', to: 'clubs#users', as: 'club_members'
  get '/club/:id/users', to: 'clubs#users', as: 'club_users'
  get '/club/:id/settings', to: 'clubs#settings', as: 'club_settings'
  patch '/club/:id/settings', to: 'clubs#settings_patch', as: 'club_settings_patch'
  post '/club/:id/comment', to: 'clubs#comment', as: 'club_comment'
  get '/club/:id/announcements', to: 'clubs#announcement', as: 'club_announcement'
  post '/club/:id/announcements', to: 'clubs#announcement_new', as: 'club_announcement_new'
  get '/club/:id/announcements/edit/:aid', to: 'clubs#announcement_edit', as: 'club_announcement_edit'
  patch '/club/:id/announcements/edit/:aid', to: 'clubs#announcement_patch', as: 'club_announcement_patch'
  get '/club/:id/announcements/:aid', to: 'clubs#announcement_show', as: 'club_announcement_show'
  delete '/club/:id/announcements/delete/:aid', to: 'clubs#announcement_delete', as: 'club_announcement_delete'
  get '/club/:id/membership/:uid', to: 'clubs#membership', as: 'club_membership'

  get '/policy/art/agreement', to: 'index#aaa', as: 'art_agreement'
  get '/policy/submission', to: 'index#subpolicy', as: 'submission_policy'
  get '/policy/terms', to: 'index#tos', as: 'terms_policy'

  get '/admin/users', to: 'admin#users', as: 'admin_users'
  post '/admin/users', to: 'admin#users_search', as: 'admin_users_search'

end
