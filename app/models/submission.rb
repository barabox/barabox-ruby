class Submission < ApplicationRecord
  belongs_to :user
  has_many :submission_comments
  has_many :favourites_users
  has_one :report
  mount_uploader :file_name, SubmissionUploader

  # Validation
  validates :file_name, presence: true
  validates :title, presence: true, length: {minimum: 3, maximum: 32}, allow_nil: false
  validates :description, presence: true, allow_nil: false
  validates :tags, presence: true, allow_nil: true
  # validates :filters, presence: true, allow_nil: true

end
