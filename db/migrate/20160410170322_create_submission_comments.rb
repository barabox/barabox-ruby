class CreateSubmissionComments < ActiveRecord::Migration[5.0]
  def change
    create_table :submission_comments do |t|
      t.belongs_to  :user
      t.belongs_to  :submission
      t.text  :comment
      t.timestamps
    end
  end
end
