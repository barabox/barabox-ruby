class CreateUserWatches < ActiveRecord::Migration[5.0]
  def change
    create_table :user_watches do |t|
      t.belongs_to :user
      t.integer :uid
      t.timestamps
    end
  end
end
