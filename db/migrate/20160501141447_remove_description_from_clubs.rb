class RemoveDescriptionFromClubs < ActiveRecord::Migration[5.0]
  def change
    remove_column :clubs, :description, :text
  end
end
