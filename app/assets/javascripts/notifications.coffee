# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('#deleteMessages').click ->
    $('#messagesForm').submit()

  $('#deleteAllMessages').click ->
    $("#deleteAllMessages").val(1)
    $('#messagesForm').submit()

  $('#selectAllMessages').click ->
    $('.msg-cb').each ->
      checked = $(this).find('input:checkbox').prop('checked')
      $(this).find('input:checkbox').prop('checked', !checked)
      if checked
        $('#selectAllMessages').html 'Select All'
      else
        $('#selectAllMessages').html 'Select None'
      return
    return
  return
