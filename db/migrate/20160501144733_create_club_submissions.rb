class CreateClubSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :club_submissions do |t|
      t.integer :club_id
      t.integer :submission_id
      t.timestamps
    end
  end
end
