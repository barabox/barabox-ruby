module MailboxHelper

  # 0 = Report, 1 = Watch, 2 = Comment, 3 = Note, 4 = Journal, 5 = Art, 6 = Poll, 7 = News, 8 = Favourite

  def mbCommentCount
    return @current_user.notifications.where('tid = ?', 2).count
  end

  def mbWatchCount
    return @current_user.notifications.where('tid = ?', 1).count
  end

  def mbNewsCount
    return @current_user.notifications.where('tid = ?', 7).count
  end

  def mbTotalCount
    @count = @current_user.notifications.count
    @count = @count + @current_user.messages.where('deleted = ? AND read = ?', false, false).count
    return ((@count > 9999) ? '9999+' : @count)
  end
end
