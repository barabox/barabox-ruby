class AddPatreonToSubmissions < ActiveRecord::Migration[5.0]
  def change
    add_column :submissions, :patreon, :boolean
  end
end
