class AddSlugToSubmissions < ActiveRecord::Migration[5.0]
  def change
    add_column :submissions, :slug, :string
  end
end
