module MathHelper
  def percentage(i,x)
    return ((i.to_f / x.to_f) * 100).round
  end
end
