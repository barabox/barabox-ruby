class ClubsController < ApplicationController

  before_action :require_login, :except => ['show', 'gallery', 'users']

  include SessionHelper
  include ClubsHelper
  include PaginationHelper

  def index
    @clubs = @current_user.clubs
  end

  def show
    @club = Club.find_by_id(params[:id])
    @cs = ClubSubmission.where('club_id = ?', @club.id).order(created_at: :desc).limit(1)
    @members = @club.club_users
    @c = ClubComment.new
    @a = @club.club_announcements.order(created_at: :desc).limit(1)
    paginationCreate(10, ClubComment, 'club_id', @club.id)
  end

  def edit
  end

  def create
    @club = Club.new(club_params)
    @club.owner_id = @current_user.id
    if @club.save
      @ce = @club.create_club_extend(club_id: @club.id, avatar: params[:club][:avatar], description: params[:club][:description])
      @club.club_users.create(user_id: @current_user.id, club_id: @club.id)
      redirect_to club_show_path @club.id
    end
  end

  def new
    @club = Club.new
  end

  def delete
  end

  def gallery
    @club = Club.find_by_id(params[:id])
    @galleries = ClubSubmission.where('club_id = ?', params[:id]).order(created_at: :desc)
  end

  def users
    @club = Club.find_by_id(params[:id])
    @users = @club.club_users
  end

  def settings
    @club = Club.find_by(id: params[:id])
  end

  def club_params
    params.require(:club).permit(:name, :description)
  end

  def comment
    @c = ClubComment.new(comment_params)
    @c.save
    redirect_to club_show_path(params[:id])
  end

  def comment_params
    params.require(:club_comment).permit(:club_id, :user_id, :comment)
  end

  def announcement
    @club = Club.find_by_id(params[:id])
    @announcement = ClubAnnouncement.new
    @announcements = ClubAnnouncement.where('club_id = ?', @club.id).order(created_at: :desc).limit(10)
  end

  def announcement_new
    @a = ClubAnnouncement.new(announcement_params)
    @a.save
    redirect_to club_announcement_path(params[:club_announcement][:club_id])
  end

  def announcement_edit
    @club = Club.find_by_id(params[:id])
    @announcements = ClubAnnouncement.where('club_id = ?', @club.id).order(created_at: :desc).limit(10)
    @a = ClubAnnouncement.find_by(id: params[:aid])
  end

  def announcement_patch
    @a = ClubAnnouncement.find_by(id: params[:aid])
    @a.update_attributes(announcement_params)
    redirect_to club_announcement_path(params[:club_announcement][:club_id])
  end

  def announcement_delete
    @a = ClubAnnouncement.find_by(id: params[:aid])
    @a.delete
    redirect_to club_announcement_path(params[:id])
  end

  def announcement_show
    @club = Club.find_by_id(params[:id])
    @a = ClubAnnouncement.find_by(id: params[:aid])
  end

  def announcement_params
    params.require(:club_announcement).permit(:title, :text, :club_id)
  end

  def settings_patch
    @club = Club.find_by_id(params[:id])
    @club.update_attribute('name', params[:club][:name])
    @club.club_extend.update_attribute('description', params[:club][:description])
    @club.club_extend.update_attribute('profile', params[:club][:profile])
    @club.club_extend.update_attribute('avatar', params[:club][:avatar])
    redirect_to club_settings_path(@club.id)
  end

  def membership
    uid = params[:uid]
    cid = params[:id]

    @c = Club.find_by_id(cid)
    if !isMember?
      @c.club_users.where('user_id = ?', uid).delete_all
    else
      @c.club_users.create(user_id: uid)
    end
    redirect_to club_show_path(cid)
  end

end
