require 'test_helper'

class JournalControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get journal_index_url
    assert_response :success
  end

  test "should get view" do
    get journal_view_url
    assert_response :success
  end

  test "should get edit" do
    get journal_edit_url
    assert_response :success
  end

  test "should get update" do
    get journal_update_url
    assert_response :success
  end

  test "should get new" do
    get journal_new_url
    assert_response :success
  end

  test "should get delete" do
    get journal_delete_url
    assert_response :success
  end

end
