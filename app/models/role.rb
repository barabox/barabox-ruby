class Role < ApplicationRecord
  has_many :users, :through => :role_users
  has_many :permissions, :through => :role_permissions
end
