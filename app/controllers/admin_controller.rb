class AdminController < ApplicationController

  include SessionHelper
  include PermissionHelper

  before_action :require_login
  before_action :checkAdmin?

  def index
  end

  def users

  end

  def users_search
    @users = User.where('LOWER(username) LIKE ?', "%#{params[:user_search][:username]}%")
  end

end
