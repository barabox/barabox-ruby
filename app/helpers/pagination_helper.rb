module PaginationHelper
  def paginationCreate(max,model,search,value)
    @maxRecords = max
    @offset = (params[:offset].nil?) ? 0 : params[:offset]
    @comments = model.where("#{search} = ?", value).limit(@maxRecords).offset(@offset)
    @count = model.where("#{search} = ?", value).count

    @totalPages = (@count / @maxRecords)
    @currentPage = @offset.to_i / @maxRecords
    @previousPage = ((@offset.to_i - @maxRecords) >= 0) ? @offset.to_i - @maxRecords : 0
    @nextPage = ((@offset.to_i + @maxRecords) <= @count) ? @offset.to_i + @maxRecords : @count
    @maxPages = ((@currentPage <= @totalPages)) ? @currentPage + 3 : @totalPages
    @startPage = 0
    @lastPage = @totalPages * @maxRecords
  end

  def genPaginationCreate(max,model)
    @maxRecords = max
    @offset = (params[:offset].nil?) ? 0 : params[:offset]
    @count = model.all.count

    @totalPages = (@count / @maxRecords)
    @currentPage = @offset.to_i / @maxRecords
    @previousPage = ((@offset.to_i - @maxRecords) >= 0) ? @offset.to_i - @maxRecords : 0
    @nextPage = ((@offset.to_i + @maxRecords) <= @count) ? @offset.to_i + @maxRecords : @count
    @maxPages = ((@currentPage <= @totalPages)) ? @currentPage + 3 : @totalPages
    @startPage = 0
    @lastPage = @totalPages * @maxRecords
  end
end
