module PollHelper
  def userVoted?(poll_id,user_id)
    @poll = Poll.find_by_id(poll_id)
    @pcc = @poll.poll_users.where('user_id = ?',user_id).count
    if @pcc > 0
      return true
    else
      return false
    end
  end

  def ownsPoll?(poll_id)
    if !logged_in?
      return false
    end
    @poll = Poll.find_by_id(poll_id)
    if @poll.user_id === @current_user.id
      return true
    else
      return false;
    end
  end

  def ownsPage?

    username = params[:user]

    if !logged_in?
      return false
    end

    if username === @current_user.username
      return true
    end
    return false
  end

end
