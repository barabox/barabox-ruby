class CreatePolls < ActiveRecord::Migration[5.0]
  def change
    create_table :polls do |t|
      t.belongs_to  :user
      t.string  :title
      t.text  :text
      t.text  :options
      t.text  :values
      t.timestamps
    end
  end
end
