module PermissionHelper
  def isAdmin?
    !@current_user.roles.find_by(name: 'admin').nil?
  end
end
