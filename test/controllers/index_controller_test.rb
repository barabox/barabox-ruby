require 'test_helper'
require 'digest'

class IndexControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "register" do
    @user = User.new(username: "baraboxio", password_digest: "password", email: "jmiller@barabox.io")
    assert @user.save
    assert @user.role_users.create(user_id: 1, role_id: 5)
    assert @user.create_user_extend(user_id: 1)
  end
end
