class CreateSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :submissions do |t|
      t.belongs_to  :user

      t.string  :title
      t.text  :description

      # Ratings
      t.boolean :r18_rating
      t.boolean :general_rating

      # Tags
      t.string  :tags
      t.string  :filters

      # Stats
      t.integer :views
      t.integer :favourites
      t.integer :downloads
      t.integer :comments

      # Image Details
      t.string  :file_name
      t.string  :file_name_thumbnail
      t.string  :file_name_thumbboard
      t.float :size
      t.string  :resolution

      # Reporting Info
      t.boolean :reported

      t.timestamps
    end
  end
end
