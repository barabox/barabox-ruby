module NotificationHelper

  # 0 = Report, 1 = Watch, 2 = Comment, 3 = Note, 4 = Journal, 5 = Art, 6 = Poll, 7 = News, 8 = Favourite

  def createNotification(type = 0, data = nil, notify = 0)
    @n = Notification.new
    @n.user_id = notify
    @n.tid = type
    @n.data = data.to_json
    @n.save
  end

  def deleteNotification(type = 0, data = nil)
    @n = Notification.where('data = ? AND tid = ?', data.to_json, type).destroy_all
  end

  def notificationCount(count)
    if count > 99
      return '99+'
    else
      return count
    end
  end


  def displayReportStatus(sid = 0)
    if sid.nil?
      sid = 0
    end

    statusName = nil
    statusType = nil

    case sid
      when 0
        statusName = 'Queued'
        statusType = 'default'
      when 1
        statusName = 'Pending'
        statusType = 'primary'
      when 2
        statusName = 'Approved'
        statusType = 'success'
      when 3
        statusName = 'Denied'
        statusType = 'danger'
      when 4
        statusName = 'Follow Up'
        statusType = 'info'
      when 5
        statusName = 'Warning'
        statusType = 'warning'
    end


    return content_tag(:span, statusName, class:['label', "label-#{statusType}"])

  end

end
