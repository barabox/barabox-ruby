class AddProfileToClubExtend < ActiveRecord::Migration[5.0]
  def change
    add_column :club_extends, :profile, :string
  end
end
