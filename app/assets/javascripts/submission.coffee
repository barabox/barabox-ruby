# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  readImage = undefined
  $('[data-toggle="tooltip"]').tooltip()
  $('textarea').autosize()
  $('#upload-file').click (e) ->
    e.preventDefault()
    $('#file-upload-select').trigger 'click'
    false
  $('#tags').tagsInput
    width: '100%'
    defaultText: 'Tags'
    placeholderColor: '#888'

  readImage = (input) ->
    canvas = undefined
    ctx = undefined
    image = undefined
    maxHeight = undefined
    maxWidth = undefined
    ratio = undefined
    reader = undefined
    maxHeight = 350
    maxWidth = 350
    ratio = 0
    canvas = $('#imgthumbcanvas')[0]
    ctx = $('#imgthumbcanvas')[0].getContext('2d')
    image = new Image
    if input.files and input.files[0]
      reader = new FileReader

      reader.onload = (e) ->

        image.onload = ->
          if image.width > maxWidth
            ratio = maxWidth / image.width
            image.width = image.width * ratio
            image.height = image.height * ratio
          else if image.height > maxHeight
            ratio = maxHeight / image.height
            image.width = image.width * ratio
            image.height = image.height * ratio
          $('#imgthumbcanvas').fadeIn 'slow'
          ctx.clearRect 0, 0, canvas.width, canvas.height
          canvas.width = image.width
          canvas.height = image.height
          ctx.drawImage image, 0, 0, image.width, image.height
          return

        image.src = e.target.result

      reader.readAsDataURL input.files[0]
    return

  $('#file-upload-select').change ->
    readImage this
    return
  $('.apps').click (e) ->
    input = undefined
    input = $('input[name="submission[' + $(this).attr('id') + ']"')
    if $(this).hasClass('disabled')
      $(this).removeClass('disabled').addClass 'enabled'
      $(input).val 1
    else
      $(this).removeClass('enabled').addClass 'disabled'
      $(input).val 0
    $('#downloadSubmission').click (e) ->
      href = undefined
      id = undefined
      url = undefined
      id = $(this).attr('data-id')
      href = $(this).attr('href')
      url = $(this).attr('data-url')
      $.get url, (data) ->
        location.href = href
        return
      return
    return
  return

$(document).on 'turbolinks:load', ->
  div = $(this).find('.comment-data')
  $('.comment-display').each ->
    if $(this).height() > 250
      div.addClass('comment-hide')
      div.find('.reveal-comment').removeClass('hide').addClass('show')
  $('.reveal-comment').click (e) ->
    e.preventDefault()
    $('.comment-data[data-id='+$(this).data('comment-id')+']').removeClass('comment-hide')
    $(this).remove()
    div.find('.reveal-comment').removeClass('show')
    return false
    return
  return

$(document).on 'turbolinks:load', ->
  $('.club-select').click (e) ->
    check = undefined
    e.preventDefault
    $('input[type=checkbox]', this).prop 'checked', !$('input[type=checkbox]', this).prop('checked')
    check = $('input[type=checkbox]', this).prop('checked')
    if check
      $(this).addClass 'selected'
    else
      $(this).removeClass 'selected'
    return
  return