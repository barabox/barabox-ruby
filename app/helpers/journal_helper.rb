module JournalHelper
  def ownsPage?

    username = params[:user]

    if !logged_in?
      return false
    end

    if username === @current_user.username
      return true
    end
    return false
  end
end
